package be.kdg.minesweeper.model.scores;

import be.kdg.minesweeper.model.Difficulty;
import be.kdg.minesweeper.model.MinesweeperException;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class ScoreManager {
    private static final String HIGH_SCORE_FILE = "highscores.bin";

    private final Map<Difficulty, Score> highScores;
    private int gamesPlayed;
    private int gamesWon;

    public ScoreManager() {
        this.highScores = new HashMap<>();
        this.gamesPlayed = 0;
        this.gamesWon = 0;

        this.loadFile();
    }

    private void saveFile() throws MinesweeperException {
        Path path = Paths.get(HIGH_SCORE_FILE);
        if (!Files.exists(path)) {
            try {
                Files.createFile(path);
            } catch (IOException e) {
                throw new MinesweeperException("Can't create high score file " + HIGH_SCORE_FILE + ".");
            }
        }

        try (DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path.toFile())))) {
            dataOutputStream.write(gamesPlayed);
            dataOutputStream.write(gamesWon);
            for (Difficulty difficulty : highScores.keySet()) {
                dataOutputStream.writeUTF(difficulty.name());
                Score score = highScores.get(difficulty);
                dataOutputStream.write(score.getSeconds());
                dataOutputStream.writeShort(score.getDate().getYear());
                dataOutputStream.writeByte(score.getDate().getMonth().getValue());
                dataOutputStream.writeByte(score.getDate().getDayOfMonth());
            }
        } catch (IOException e) {
            throw new MinesweeperException("Error while writing to " + HIGH_SCORE_FILE + ".");
        }
    }

    private void loadFile() {
        Path path = Paths.get(HIGH_SCORE_FILE);
        if (Files.exists(path)) {
            try (DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(path.toFile())))) {
                gamesPlayed = dataInputStream.read();
                gamesWon = dataInputStream.read();
                while (dataInputStream.available() > 0) {
                    Difficulty difficulty = Difficulty.get(dataInputStream.readUTF());
                    int score = dataInputStream.read();
                    short year = dataInputStream.readShort();
                    byte month = dataInputStream.readByte();
                    byte day = dataInputStream.readByte();
                    highScores.put(difficulty, new Score(score, LocalDate.of(year, month, day)));
                }
            } catch (IOException e) {
                throw new MinesweeperException("Can't create high score file " + HIGH_SCORE_FILE + ".");
            }
        }
    }

    public Score getHighScore(Difficulty difficulty) {
        return highScores.get(difficulty);
    }

    public void gamePlayed(Difficulty difficulty, Score newScore) {
        gamesPlayed++;
        if (newScore != null) {
            gamesWon++;

            Score existingScore = getHighScore(difficulty);
            if (existingScore == null || newScore.getSeconds() < existingScore.getSeconds()) {
                highScores.put(difficulty, newScore);
            }
        }
        saveFile();
    }

    public int getGamesPlayed() {
        return this.gamesPlayed;
    }

    public int getGamesWon() {
        return this.gamesWon;
    }
}
