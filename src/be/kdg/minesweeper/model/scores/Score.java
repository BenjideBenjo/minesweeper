package be.kdg.minesweeper.model.scores;

import java.time.LocalDate;

public class Score {
    private final int seconds;
    private final LocalDate date;

    public Score(int seconds) {
        this(seconds, LocalDate.now());
    }

    Score(int seconds, LocalDate date) {
        this.seconds = seconds;
        this.date = date;
    }

    public int getSeconds() {
        return seconds;
    }

    public LocalDate getDate() {
        return date;
    }
}
