package be.kdg.minesweeper.model;

import be.kdg.minesweeper.model.scores.ScoreManager;
import be.kdg.minesweeper.model.scores.Score;

import java.util.List;

public class Game {
    private final Grid grid;
    private final ScoreManager scoreManager;
    private boolean isStarted;
    private boolean isFinished;
    private Score score;
    private int seconds;

    public Game(Difficulty difficulty) {
        this.grid = new Grid(difficulty);
        this.scoreManager = new ScoreManager();
        this.score = null;
        this.isStarted = false;
        this.isFinished = false;
        this.seconds = 0;
    }

    public Difficulty getDifficulty() {
        return grid.getDifficulty();
    }

    private Grid getGrid() {
        return this.grid;
    }

    private void checkFinished() {
        if (this.getGrid().getRowCount() * this.grid.getColumnCount() - this.grid.getDifficulty().getBombs() == this.grid.getNumberOfRevealedCells()) {
            this.isFinished = true;
            this.score = new Score(this.seconds);
            this.scoreManager.gamePlayed(this.getDifficulty(), this.score);
        }
    }

    public List<Cell> play(int row, int column) {
        if (!this.isFinished) {
            this.isStarted = true;
            Cell targetCell = getGrid().getCell(row, column);
            if (targetCell.hasBomb()) {
                this.isFinished = true;
                this.scoreManager.gamePlayed(this.getDifficulty(), null);
                return this.grid.getAllBombs();
            } else {
                List<Cell> cellsToReveal = this.grid.getCellsToReveal(row, column);
                checkFinished();
                return cellsToReveal;
            }
        } else {
            throw new MinesweeperException("Spel is al afgelopen.");
        }
    }

    public Cell getCell(int row, int column) {
        return getGrid().getCell(row, column);
    }

    public void changePrediction(int row, int column) {
        if (!this.isFinished) {
            Cell targetCell = getGrid().getCell(row, column);
            if (!targetCell.isRevealed()) {
                Prediction newPrediction;
                if (targetCell.getPrediction() == Prediction.NONE) {
                    newPrediction = Prediction.FLAG;
                } else if (targetCell.getPrediction() == Prediction.FLAG) {
                    newPrediction = Prediction.MAYBE;
                } else {
                    newPrediction = Prediction.NONE;
                }
                targetCell.setPrediction(newPrediction);
            } else {
                throw new MinesweeperException("Kan geen voorspelling maken voor een getoond vakje.");
            }
        } else {
            throw new MinesweeperException("Spel is al afgelopen.");
        }
    }

    public boolean isRevealed(int row, int column) {
        return this.getGrid().getCell(row, column).isRevealed();
    }

    public boolean isStarted() {
        return isStarted;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public boolean isWon() {
        return this.score != null;
    }

    public int getNeighbouringBombs(int row, int column) {
        return this.getGrid().getNeighbouringBombs(row, column);
    }

    public void increaseTimer() {
        this.seconds++;
    }

    public int getSeconds() {
        return seconds;
    }

    public ScoreManager getScoreManager() {
        return scoreManager;
    }
}
