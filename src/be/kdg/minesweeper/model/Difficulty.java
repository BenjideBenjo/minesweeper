package be.kdg.minesweeper.model;

public enum Difficulty {
    EASY(9, 9, 10) /* 8, 8, 10 before Win2000 */, MEDIUM(16, 16, 40), HARD(30, 16, 99);

    private final int width;
    private final int height;
    private final int bombs;

    Difficulty(int width, int height, int bombs) {
        this.width = width;
        this.height = height;
        this.bombs = bombs;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getBombs() {
        return this.bombs;
    }

    public static Difficulty get(String name) {
        for (Difficulty difficulty : values()) {
            if (difficulty.name().equals(name)) {
                return difficulty;
            }
        }
        return null;
    }
}
