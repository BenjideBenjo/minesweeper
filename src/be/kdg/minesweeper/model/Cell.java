package be.kdg.minesweeper.model;

public class Cell {
    private final int row;
    private final int column;
    private boolean hasBomb;
    private boolean isRevealed;
    private Prediction prediction;

    Cell(int row, int column) {
        this.row = row;
        this.column = column;
        this.hasBomb = false;
        this.prediction = Prediction.NONE;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean hasBomb() {
        return hasBomb;
    }

    void setHasBomb() {
        this.hasBomb = true;
    }

    public boolean isRevealed() {
        return isRevealed;
    }

    void setRevealed() {
        this.isRevealed = true;
    }

    public Prediction getPrediction() {
        return prediction;
    }

    void setPrediction(Prediction prediction) {
        if (prediction == null) {
            throw new RuntimeException("Set prediction to NONE instead of null.");
        }
        this.prediction = prediction;
    }
}
