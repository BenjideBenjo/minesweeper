package be.kdg.minesweeper.view.gameover;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class GameOverView extends GridPane {
    private Label congratulationsLabel;
    private Label timeLabel;
    private Label bestTimeLabel;
    private Label dateLabel;
    private Label gamesPlayedLabel;
    private Label gamesWonLabel;
    private Label percentageLabel;

    private Button playAgainButton;
    private Button exitButton;

    public GameOverView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.congratulationsLabel = new Label();
        this.timeLabel = new Label();
        this.bestTimeLabel = new Label();
        this.dateLabel = new Label();
        this.gamesPlayedLabel = new Label();
        this.gamesWonLabel = new Label();
        this.percentageLabel = new Label();
        this.playAgainButton = new Button("Play again!");
        this.exitButton = new Button("Exit");
    }

    private void layoutNodes() {
        //this.setGridLinesVisible(true);
        this.setPadding(new Insets(15.0));
        this.setHgap(20.0);
        this.setVgap(20.0);

        this.add(this.congratulationsLabel, 0, 0, 2, 1);
        this.add(this.timeLabel, 0, 1);
        this.add(this.bestTimeLabel, 0, 2);
        this.add(this.dateLabel, 1, 2);
        this.add(this.gamesPlayedLabel, 0, 3);
        this.add(this.gamesWonLabel, 0, 4);
        this.add(this.percentageLabel, 1, 4);

        this.add(this.playAgainButton, 0, 5);
        GridPane.setConstraints(this.playAgainButton, 0, 5, 1, 1, HPos.CENTER, VPos.BOTTOM);
        this.add(this.exitButton, 1, 5);
        GridPane.setConstraints(this.exitButton, 1, 5, 1, 1, HPos.CENTER, VPos.BOTTOM);
    }

    Button getPlayAgainButton() {
        return playAgainButton;
    }

    Button getExitButton() {
        return exitButton;
    }

    Label getCongratulationsLabel() {
        return congratulationsLabel;
    }

    Label getTimeLabel() {
        return timeLabel;
    }

    Label getBestTimeLabel() {
        return bestTimeLabel;
    }

    Label getDateLabel() {
        return dateLabel;
    }

    Label getGamesPlayedLabel() {
        return gamesPlayedLabel;
    }

    Label getGamesWonLabel() {
        return gamesWonLabel;
    }

    Label getPercentageLabel() {
        return percentageLabel;
    }
}
