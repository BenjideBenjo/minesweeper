package be.kdg.minesweeper.view.sound;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class SoundPlayer {
    private static final Media BLIP_SOUND = new Media(SoundPlayer.class.getClassLoader().getResource("sound/blip.mp3").toExternalForm());
    private static final Media EXPLOSION_SOUND = new Media(SoundPlayer.class.getClassLoader().getResource("sound/explosion.mp3").toExternalForm());
    private static final Media REVEAL_SOUND = new Media(SoundPlayer.class.getClassLoader().getResource("sound/reveal.mp3").toExternalForm());

    public static void playBlipSound() {
        final MediaPlayer player = new MediaPlayer(BLIP_SOUND);
        player.play();
    }

    public static void playExplosionSound() {
        final MediaPlayer player = new MediaPlayer(EXPLOSION_SOUND);
        player.play();
    }

    public static void playRevealSound() {
        final MediaPlayer player = new MediaPlayer(REVEAL_SOUND);
        player.play();
    }
}
