package be.kdg.minesweeper.view;

import be.kdg.minesweeper.model.Cell;
import javafx.scene.image.Image;

public enum CachedImage {
    UNKNOWN("images/unknown.png"),
    FLAG("images/flag.png"),
    MAYBE("images/maybe.png"),
    BOMB("images/bomb.png"),
    EMPTY_0("images/empty_0.png"),
    EMPTY_1("images/empty_1.png"),
    EMPTY_2("images/empty_2.png"),
    EMPTY_3("images/empty_3.png"),
    EMPTY_4("images/empty_4.png"),
    EMPTY_5("images/empty_5.png"),
    EMPTY_6("images/empty_6.png"),
    EMPTY_7("images/empty_7.png"),
    EMPTY_8("images/empty_8.png");

    private final Image image;

    CachedImage(String url) {
        this.image = new Image(url);
    }

    public Image getImage() {
        return this.image;
    }

    public static CachedImage get(Cell cell, int neighbouringBombs) {
        if (!cell.isRevealed()) {
            switch (cell.getPrediction()) {
                case FLAG:
                    return FLAG;
                case MAYBE:
                    return MAYBE;
                default:
                    return UNKNOWN;
            }
        } else if (cell.hasBomb()) {
            return BOMB;
        } else {
            switch (neighbouringBombs) {
                case 0:
                    return EMPTY_0;
                case 1:
                    return EMPTY_1;
                case 2:
                    return EMPTY_2;
                case 3:
                    return EMPTY_3;
                case 4:
                    return EMPTY_4;
                case 5:
                    return EMPTY_5;
                case 6:
                    return EMPTY_6;
                case 7:
                    return EMPTY_7;
                default:
                    return EMPTY_8;
            }
        }
    }
}
